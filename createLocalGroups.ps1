
#------ Opprette Local Groups ---------#

Get-Help New-ADGroup -Online

$newLocalGroups = @('dev', 'finance', 'hr', 'it', 'sale')

foreach($group in $newLocalGroups) {
    New-ADGroup `
        -Name "I_remotedesktop_$group" `
        -SamAccountName "I_remotedesktop_$group" `
        -GroupCategory Security `
        -GroupScope Global `
        -DisplayName "I_remotedesktop_$group" `
        -Path "OU=MonTech_Groups,DC=core,DC=sec" `
        -Description "$group Local Group"
}


Add-ADPrincipalGroupMembership -Identity 'g_hr' -MemberOf 'I_remotedesktop_hr'
