Add-WindowsCapability -Name Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0 -Online

Get-Command -Module ActiveDirectory | Where-Object {$_.name -like "*user*"}
Get-Command -Module ActiveDirectory | Where-Object {$_.name -like "*group*"}
Get-Command -Module ActiveDirectory | Where-Object {$_.name -like "*OrganizationalUnit*"}

# 1. Opprette OU-struktur i AD for brukere, maskiner og grupper
# 2. Opprette grupper som skal representere en eksempelbedrift
# 3. Opprette brukere som skal:
#       -Plasseres i onsket OU
#       -Meldes inn i tiltenkt gruppe for tilganger
#       -Sorge for at de faar logget seg inn paa onsket maskin (cl1)


# Eksemplebedrift - MonTech
$MT_users = "MonTech_Users"
$MT_groups = "MonTech_Groups"
$MT_computers = "MonTech_Computers"

$topOUs = $($MT_users, $MT_groups, $MT_computers)
$departments = @('hr', 'it', 'dev', 'sale', 'finance')

foreach($ou in $topOUs) {
    New-ADOrganizationalUnit $ou -Description "Top OU for MonTech" -ProtectedFromAccidentalDeletion:$false
    $topOU = Get-ADOrganizationalUnit -Filter * | Where-Object {$_.name -eq "$ou"}
    #Get-ADOrganizationalUnit -Filter * | Where-Object {$_.name -eq $ou} | Remove-ADOrganizationalUnit -Recursive -Confirm:$false
        foreach($department in $departments) {
            New-ADOrganizationalUnit $department `
            -Path $topOU.DistinguishedName ` 
            -Description "Department OU for $department in $topOU" `
            -ProtectedFromAccidentalDeletion:$false
        }
}

#------- Group Structure ------#
Get-Help New-ADGroup -Online



foreach ($department in $departments) {
    $path = Get-ADOrganizationalUnit -Filter * | `
            Where-Object {($_.name -eq "$department")`
            -and ($_.DistinguishedName -like "OU=$department,OU=$MT_groups,*")}
    New-ADGroup -Name "g_$department" `
        -SamAccountName "g_$department" `
        -GroupCategory Security `
        -GroupScope Global `
        -DisplayName "g_$department" `
        -Path $path.DistinguishedName `
        -Description "$department Group"
}

New-ADGroup -Name "g_all_employee" `
            -SamAccountName "g_all_employee" `
            -GroupCategory Security `
            -GroupScope Global `
            -DisplayName "g_all_employee" `
            -path "OU=MonTech_Groups,DC=core,DC=sec" `
            -Description "all emplyoee"



#----- Create User ------#
# The easy way#

Get-Help New-ADUser -online

$Password = Read-Host -Prompt "Enter password" -AsSecureString
New-ADUser -Name "Jorn Johnsen" `
        -GivenName "Jorn" `
        -Surname "Johnsen" `
        -SamAccountName "jjohnsen" `
        -UserPrincipalName "jjohnsen@core.sec" `
        -Path "OU=hr,OU=MonTech_Users,DC=core,DC=sec" `
        -AccountPassword $Password -Enabled $true


# The easy way Part 2#

$users = Import-Csv -Path 'C:\projects\dcst1005-new1\users.csv' -Delimiter ";" 


foreach ($user in $users) {
    New-ADUser -Name $user.DisplayName `
    -GivenName $user.GivenName `
    -Surname $user.Surname `
    -SamAccountName $user.Username `
    -UserPrincipalName $user.UserPrincipalName `
    -Path $user.Path `
    -AccountPassword (convertto-securestring $user.Password -AsPlainText -Force) `
    -Department $user.Department `
    -Enabled $true
}

foreach ($user in $users) {
    Remove-ADUser -Identity $user.Username -Confirm:$false
}


# Hard Way + Brukere med særnorkse og vasking / formatering av data, randomgenerering av passord #

$users = Import-Csv -Path 'C:\projects\dcst1005-new1\users_advanced.csv' -Delimiter ";"
$csvfile = @()
$exportUserPath = 'C:\projects\dcst1005-new1\user_final.csv'
$exportPathFinal = 'C:\projects\dcst1005-new1\users_final_v2.csv'

34..126 -as [char[]]

function New-UserPassword {
    param (
        [int]$length = 12
    )
    $characterSet = 37..126 -as [char[]]
    $password = ''
    1..$length | ForEach-Object {
        $password += $characterSet | Get-Random
    }
    return $password
}

$password = New-UserPassword -length 12
$password = $password.Replace(';', '!!')


function New-UserInfo {
    param (
        [Parameter(Mandatory =$true)][string] $fornavn, 
        [Parameter(Mandatory =$true)][string] $etternavn
    )

    if ($fornavn -match $([char]32)) {
        $oppdelt = $fornavn.Split($([char]32))
        $fornavn = $oppdelt[0]

        for ( $index = 1 ; $index -lt $oppdelt.Length ; $index ++ ) {
            $fornavn += ".$($oppdelt[$index][0])"
        }
    }

    $UserPrincipalName = $("$($fornavn).$($etternavn)").ToLower()
    $UserPrincipalName = $UserPrincipalName.Replace('æ','e')
    $UserPrincipalName = $UserPrincipalName.Replace('ø','o')
    $UserPrincipalName = $UserPrincipalName.Replace('å','a')
    $UserPrincipalName = $UserPrincipalName.Replace('é','e')

    return $UserPrincipalName
    
}

foreach ($user in $users) {
     $password = New-UserPassword
     $line = New-Object -TypeName psobject

     Add-Member -InputObject $line -MemberType NoteProperty -Name GivenName -Value $user.GivenName
     Add-Member -InputObject $line -MemberType NoteProperty -Name Surname -Value $user.SurName
     Add-Member -InputObject $line -MemberType NoteProperty -Name UserPrincipalName -Value "$(New-UserInfo -Fornavn $user.GivenName -Etternavn $user.SurName)@core.sec"
     Add-Member -InputObject $line -MemberType NoteProperty -Name DisplayName -Value "$($user.GivenName) $($user.SurName)"
     Add-Member -InputObject $line -MemberType NoteProperty -Name Department -Value $user.Department
     Add-Member -InputObject $line -MemberType NoteProperty -Name Password -Value $password
     $csvfile += $line
}

$csvfile | Export-Csv -Path $exportUserPath -NoTypeInformation -Encoding "UTF8"
Import-Csv -Path $exportUserPath | ConvertTo-Csv -NoTypeInformation | ForEach-Object {$_ -Replace '"', ""} | Out-File $exportPathFinal -Encoding 'UTF8'

$users = Import-Csv -Path 'C:\projects\dcst1005-new1\users_final_v2.csv' -Delimiter ","

foreach ($user in $users) {
    $sam = $user.UserPrincipalName.Split("@")
        if ($sam[0].Length -gt 19) {
            "SAM-account-Name er for langt, bruker de 19 forste tegnene"
            $sam[0] = $sam[0].Substring(0, 19)
        }
        $sam[0]
        [string] $samaccountname = $sam[0]

        [string] $department = $user.Department
        [string] $searchdn = "OU=$department,OU=$MT_users,*"
        $path = Get-ADOrganizationalUnit -Filter * | Where-Object {($_.name -eq $user.Department) -and ($_.DistinguishedName -like $searchdn)}

        if (!(Get-ADUser -Filter "samAccountName -eq '$($samaccountname)'")) {
            Write-Host "$samaccountname does not exist." -ForegroundColor Green
            Write-Host "Creating User ....%" -ForegroundColor Green
            Write-Host $user.DisplayName -ForegroundColor Green
            
            New-ADUser `
            -SamAccountName $samaccountname `
            -UserPrincipalName $user.UserPrincipalName `
            -Name $samaccountname `
            -GivenName $user.GivenName `
            -Surname $user.SurName `
            -Enabled $True `
            -ChangePasswordAtLogon $False `
            -DisplayName $user.DisplayName `
            -Department $user.Department `
            -Path $path `
            -AccountPassword (ConvertTo-SecureString $user.Password -AsPlainText -Force)
        }
        #Write-Host "Bruker: $samaccountname funnet, ingen ny opprettelse" -ForegroundColor Green
}

$ADUsers = @()

foreach ($department in $departments) {
    $ADUsers = Get-ADUser -Filter {Department -eq $department} -Properties Department
    #Write-Host "$ADUsers som er funnet under $department"

    foreach ($aduser in $ADUsers) {
        #get-help Add-ADPrincipalGroupMembership -online
        Add-ADPrincipalGroupMembership -Identity $aduser.SamAccountName -MemberOf "g_$department"
    }
}

Get-ADGroupMember -Identity "g_finance"

<#Get-ADObject -Identity "OU=MonTech_Users,DC=core,DC=sec" | Set-ADObject -ProtectedFromAccidentalDeletion:$false

foreach ($ou in $topOUs) {
    Remove-ADOrganizationalUnit -Identity "OU=$ou,DC=core,DC=sec" -Recursive -Confirm:$false
} #>

# ---- Create Personal Domain Admin -----#

$password = Read-Host -AsSecureString

New-ADUser `
    -SamAccountName "chstens" `
    -UserPrincipalName "chstens@core.sec" `
    -Name "Christian Stensoee" `
    -GivenName "Christian" `
    -Surname "Stensoee" `
    -Enabled $True `
    -ChangePasswordAtLogon $False `
    -DisplayName "Christian Stensoee" `
    -Department "it" `
    -Path "OU=it,OU=MonTech_Users,DC=core,DC=sec" `
    -AccountPassword $password


    Add-ADPrincipalGroupMembership -Identity 'chstens' -MemberOf "Administrators"
    Add-ADPrincipalGroupMembership -Identity 'chstens' -MemberOf "Domain Admins"


    #------Move Computer to correct OU------#

    Get-ADComputer -Filter * | ft

    Move-ADObject -Identity "CN=mgr,CN=Computers,DC=core,DC=sec" `
            -TargetPath "OU=it,OU=MonTech_Computers,DC=core,DC=sec"


New-ADOrganizationalUnit "Server" `
                    -Description "OU for Servers" `
                    -Path "OU=MonTech_Computers,DC=core,DC=sec" `
                    -ProtectedFromAccidentalDeletion:$false

