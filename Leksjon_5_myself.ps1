Get-WindowsFeature -ComputerName srv1
Get-WindowsFeature -ComputerName srv1 | Where-Object {$_. installstate -eq "installed"}

Install-WindowsFeature -Name FS-DFS-Namespace, `
                        FS-DFS-Replication,RSAT-DFS-Mgmt-Con `
                        -ComputerName srv1 `
                        -IncludeManagementTools

Invoke-Command -ComputerName srv1 -ScriptBlock {New-Item -Path "C:\" -Name 'dfsroot' -ItemType "directory"}
Invoke-Command -ComputerName srv1 -ScriptBlock {New-Item -Path "C:\" -Name "shares" -ItemType "directory"}
Invoke-Command -ComputerName srv1 -ScriptBlock {mkdir "C:\shares\it"}

Enter-PSSession -ComputerName srv1
# Oppretter mapper for share of DFS Root folder.
# In DFS term the Root is a share and a Link is a virtual Foler name to a remote server Share/folder.
$folders = ('C:\dfsroot\files', 'C\shares\finance', `
            'C:\shares\sales', 'C:\shares\it', `
            'C:\shares\dev', 'C:\shares\hr')
mkdir -Path $folders
# Deler alle mappene slik at de er tilgjengelig paa nettverket.
$folders | ForEach-Object {$sharename = (Get-Item $_).name; New-SMBShare -Name $sharename -Path $_ -FullAccess Everyone}

# Denne kommandoen maa gjores i srv powershell. Kan IKKE gjores i PSRemote eller Invoke CMD.
 
New-DfsnRoot -TargetPath \\srv1\files -Path \\core.sec\files -Type DomainV2


<# Benytter innholdet fra $folders variabelen fra 
tidligere og peker alle delte omraader fra \\srv1\ til --> core.sec\files #>
# Tar med denne kommandosnutten til srv1 Powershell, siden det ikke vil funke over PSSession
$folders = ('C:\dfsroot\files', 'C:\shares\finance', `
            'C:\shares\sales', 'C:\shares\it', `
            'C:\shares\dev', 'C:\shares\hr')
$folders | Where-Object {$_ -like "*shares*"} | `
            ForEach-Object {$name = (Get-Item $_).name; `
                        $DfsPath = ('\\core.sec\files\' + $name); `
                        $targetPath = ('\\srv1\' + $name); New-DfsnFolderTarget `
                        -Path $DfsPath `
                        -TargetPath $targetPath}

# List ut alle folderene
dir \\core\files


# exit: Tilbake i MGR
#---------- Create a local group named 1_fullaccess_DEVshare ---------#
       
New-ADGroup -GroupCategory Security `
            -GroupScope DomainLocal `
            -Name '1_fullaccess_DEVshare' `
            -Path 'OU=MonTech_groups,DC=core,DC=sec' `
            -SamAccountName '1_fullaccess_DEVshare'

Add-ADPrincipalGroupMembership -Identity 'g_dev' -MemberOf '1_fullaccess_DEVshare'




#----- View Access Control on shared folder ------#
#  Kun for utlisting av eksisterende tilgang, gjor ingenting spesielt
Enter-PSSession -ComputerName srv1
$folders = "\\core.sec\files\dev"
Get-SmbShareAccess -Name 'dev'
Get-Acl -Path $folders
(Get-Acl -Path $folders).Access
(Get-Acl -Path $folders).Access | Format-Table -AutoSize
(Get-Acl -Path $folders).Access | Where-Object {$_.IsInherited -eq $true} | Format-Table -AutoSize
(Get-Acl -Path $folders).Access | Format-Table IdentityReference, FileSystemRights, AccessControlType, `
                                                IsInherited,InheritanceFlags -AutoSize

<# MERK at vi ikke har endret noe på filsystemet ennå. Vi har bare endret $acl-objektet


Arv
Når vi oppretter tilgangsregler, må vi også spesifisere arv. Dette gjøres ved fire alternativer. 
En vil mest sannsynlig angi både ContainerInheritance- og ObjectInheritance-flaggene.
Dette indikerer at en ønsker at tillatelsen vår skal spres til alle underordnede kataloger og filer.

ContainerInherit - Forplant til underordnede beholdere (kataloger).
None - Ikke spre denne tillatelsen til noen underordnede beholdere eller objekter.
ObjectInherit - Forplante seg til underordnede objekter (filer).

Propagation Flags
I tillegg til arveflagg kan vi også sette propagation.
Standard i Windows None.
Tre alternativene for propagation flags. 
Hvis du setter dette flagget til InheritOnly, vil tilgangsregelen kun gjelde for underordnede objekter i beholderen.

InheritOnly - Spesifiserer at ACE-en bare spres til underordnede objekter.
None - No inheritance flagg satt.
NoPropagateInherit - ACE går ikke nedover til underelementer.

Flag Combinations and Effect
The table below details how a rule will be apply to the current object and child objects based on the flags set in the access rule.


--Access Rule Applies To--              --Inheritance Flags--              --Propagation Flags--
Subfolders and Files only               ContainerInherit,ObjectInherit      InheritOnly
This Folder, Subfolders and Files       ContainerInherit,ObjectInherit	    None
This Folder, Subfolders and Files       ContainerInherit,ObjectInherit	    NoPropagateInherit
This folder and Subfolders	            ContainerInherit	                None
Subfolders only	Container               Inherit                             InheritOnly
This Folder and Files                   ObjectInherit                       None
This Folder and Files                   ObjectInherit                       NoPropagateInherit
This Folder only                        None                                None

GUI: Inheritance Flags

#>



<# 
Setter variabelen $folders til verdien "C:\shares\dev".
Henter ACL for mappen "\core\files\dev".
Lager en ny FileSystemAccessRule-regel kalt $AccessRule med identiteten 
"core\l_fullaccess_DEVshare", med full kontroll tillatelse og tillatelsen "Allow".
Legger til $AccessRule i ACL-en.
Oppdaterer ACL-en for mappen "\core\files\dev".
#>

$folders = ('C:\shares\dev')
$acl = Get-Acl \\core\files\dev
$AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("core\1_fullaccess_DEVshare", "FullControl", "Allow")
$acl.SetAccessRule($AccessRule)
$acl | Set-Acl -Path "\\core\files\dev"


<# 
Setter variabelen $folders til verdien "C:\shares\dev".
Henter ACL for mappen "\core\files\dev".
Lager en ny FileSystemAccessRule-regel kalt $AccessRule med identiteten 
"core\l_fullaccess_DEVshare", med full kontroll tillatelse og tillatelsen "Allow".
Legger til $AccessRule i ACL-en.
Oppdaterer ACL-en for mappen "\core\files\dev".
#>
$folders = ('C:\shares\dev')
$acl = Get-Acl \\core\files\dev
$AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("core\l_fullaccess_DEVshare","FullControl","Allow")
$acl.SetAccessRule($AccessRule)
$ACL | Set-Acl -Path "\\core\files\dev"

<#
Henter oppdatert ACL for mappen "\core\files\dev".
Beskytter ACL-en slik at den ikke kan endres ($ACL.SetAccessRuleProtection($true,$true)).
Oppdaterer den beskyttede ACL-en for mappen "\core\files\dev".
#>

$acl = Get-Acl -Path "\\core\files\dev"
$acl.SetAccessRuleProtection($true,$true)
$acl | Set-Acl -Path "\\core\files\dev"

<#
$acl.SetAccessRuleProtection-metoden tar to argumenter: $True og $False. 
    - De to argumentene bestemmer hvordan tilgangsreglene skal håndteres.
$acl.SetAccessRuleProtection($True,$True):
    - betyr at tilgangsregler vil bli arvet, men ikke kan endres eller slettes.
$acl.SetAccessRuleProtection($False,$False): 
    - betyr at tilgangsregler vil bli arvet og kan endres eller slettes.
$acl.SetAccessRuleProtection($True,$False):
    - betyr at tilgangsregler vil bli arvet, men kan endres. De kan ikke slettes.
$acl.SetAccessRuleProtection($False,$True)
    - betyr at tilgangsregler ikke vil bli arvet, men kan endres eller slettes.
#>


<#
Henter ACL for mappen "\core\files\dev".
Fjerner spesifikke tillatelser for gruppen "BUILTIN\Users".
Oppdaterer ACL-en for mappen "\core\files\dev".
Viser en tabell med identitetsreferanse, filsystemrettigheter, kontrolltype,
om det er arvet og arveflag for den oppdaterte ACL-en.
#>

$acl = Get-Acl "\\core\files\dev"
$acl.Access | Where-Object {$_.identityreference -eq "BUILTIN\Users"} | ForEach-Object {$acl.RemoveAccessRuleSpecific($_)}
Set-Acl "\\core\files\dev" $acl    #    $acl | Set-Acl "\\core\files\dev"
(Get-Acl -Path "\\core\files\dev").Access | `
                Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize