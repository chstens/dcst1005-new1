#### Leksjon 05 - Video 1 - GPO med RDP-tilgang for ansatte ####

New-ADGroup -GroupCategory Security `
    -GroupScope DomainLocal `
    -Name 'l_remotedesktop_finance' `
    -Path 'OU=learnIT_groups,DC=core,DC=sec' `
    -SamAccountName 'l_remotedesktop_finance'


# gir tilgang til ansatte over RDP til clientmaskiner
$clientsOU = "OU=hr,OU=LearnIT_Computers,DC=Core,DC=sec"
$rdpgroup = Get-ADGroup -identity "l_remotedesktop"
$rdpgroupSID = (New-Object System.Security.Principal.SecurityIdentifier($rdpgroup.SID)).Value
# Må kjøres i PSSession på DC1 eller invoke cmd (invoke gjør det litt tricky med variableinnhold)
# Install-WindowsFeature GPMC får følgende beskjed:
# "Install-WindowsFeature: The target of the specified cmdlet cannot be a Windows client-based operating system."


Enter-PSSession -ComputerName dc1
$GPO = New-GPO -Name "Allow Remote Desktop for HR"
New-GPLink -Name "Allow Remote Desktop for HR" -Target "OU=hr,OU=MonTech_Computers,DC=Core,DC=sec"

Add-GPORestrictedGroup -Name "Allow Remote Desktop for employees" -GPOPath $gpo.GPOFileSysPath -GroupName "Remote Desktop Users"
 

#### - Leksjon 05 - Video 2 - Installer tjenester for DFS på SRV1 ####
Get-WindowsFeature -ComputerName srv1 
Get-WindowsFeature -ComputerName srv1 | Where-Object {$_. installstate -eq "installed"}

Install-WindowsFeature -Name FS-DFS-Namespace,FS-DFS-Replication,RSAT-DFS-Mgmt-Con `
                        -ComputerName srv1 `
                        -IncludeManagementTools

Remove-WindowsFeature -Name FS-DFS-Namespace,FS-DFS-Replication,RSAT-DFS-Mgmt-Con `
-ComputerName srv1

Get-WindowsFeature -ComputerName srv1 | Where-Object {$_. installstate -eq "installed"}



Invoke-Command -ComputerName srv1 -ScriptBlock {New-Item -Path "c:\" -Name 'dfsroots' -ItemType "directory"}
Invoke-Command -ComputerName srv1 -ScriptBlock {New-Item -Path "c:\" -Name 'shares' -ItemType "directory"}
Invoke-Command -ComputerName srv1 -ScriptBlock {mkdir "c:\shares\it"}


Enter-PSSession -ComputerName srv1
# Oppretter mapper for share og DFS Root folder
# In DFS terms the Root is the share and a Link is a virtual Folder name to a remote server Share\folder.
$folders = ('C:\dfsroots\files','C:\shares\finance','C:\shares\sale','C:\shares\it','C:\shares\dev','C:\shares\hr')
mkdir -path $folders
# Deler alle mappene så de er tilgjengelige på nettverket
$folders | ForEach-Object {$sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone}

<#
Denne feiler i PSRemote og Invoke cmd: New-DfsnRoot -TargetPath \\srv1\files -Path \\core.sec\files -Type DomainV2
#>

<# LES OG GJØR:
Følgende kommando: 
    New-DfsnRoot -TargetPath \\srv1\files -Path \\core.sec\files -Type DomainV2
Gir feilmelding:
    [srv1]: PS C:\Users\Administrator\Documents> New-DfsnRoot -TargetPath \\srv1\files -Path \\core.sec\files -Type DomainV2
    New-DfsnRoot : A general error occurred that is not covered by a more specific error code.
        + CategoryInfo          : NotSpecified: (MSFT_DFSNamespace:ROOT\Microsoft\...FT_DFSNamespace) [New-DfsnRoot], 
        CimException
        + FullyQualifiedErrorId : MI RESULT 110,New-DfsnRoot

#############
Når kommandoen kjøres lokalt på srv1 (ikke PSSession) maskinen i PowerShell 7.x som administrator,
eller egen bruker som er domeneadministrator får en følgende bekreftelse:

------------------------------------------------------------------------------------

Path                     Type      Properties TimeToLiveSec State  Description
----                     ----      ---------- ------------- -----  -----------
\\core.sec\files Domain V2            300           Online

------------------------------------------------------------------------------------

Kjør kommandoen lokalt på srv1 maskinen, pass på at maskinnavn og domenenavn er korrekt.
#>

# Benytter innholdet i $folders variabelen fra tidligere og peker alle delte områdene fra \\srv1\ til --> \\core.sec\files
# Tar med denne kommandosnutten til srv1 PowerShell, får ikke ønsket utfall over pssession:
$folders = ('C:\dfsroots\files','C:\shares\finance','C:\shares\sale','C:\shares\it','C:\shares\dev','C:\shares\hr')
$folders | Where-Object {$_ -like "*shares*"} | 
            ForEach-Object {$name = (Get-Item $_).name; `
                $DfsPath = ('\\core.sec\files\' + $name); `
                $targetPath = ('\\srv1\' + $name);New-DfsnFolderTarget `
                -Path $dfsPath `
                -TargetPath $targetPath}
dir \\core\files



# --- Create local group name l_fullaccess_DEVshare --- #

New-ADGroup -GroupCategory Security `
    -GroupScope DomainLocal `
    -Name 'l_fullaccess_DEVshare' `
    -Path 'OU=learnIT_groups,DC=core,DC=sec' `
    -SamAccountName 'l_fullaccess_DEVshare'

Add-ADPrincipalGroupMembership -Identity 'g_dev' -MemberOf 'l_fullaccess_DEVshare'



# --- Access Control on shared folde --- #
# Kun for utlisting av eksisterende tilgang :)
$folders = "\\core.sec\files\dev"
Get-SmbShareAccess -name 'dev'
Get-Acl -Path $folders
(Get-Acl -Path $folders).Access
(Get-Acl -Path $folders).Access | Format-Table -AutoSize
(Get-Acl -Path $folders).Access | Where-Object {$_.IsInherited -eq $true} | Format-Table -AutoSize
(Get-ACL -Path $folders).Access | Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize

<# MERK at vi ikke har endret noe på filsystemet ennå. Vi har bare endret $acl-objektet

Arv
Når vi oppretter tilgangsregler, må vi også spesifisere arv. Dette gjøres ved fire alternativer. 
En vil mest sannsynlig angi både ContainerInheritance- og ObjectInheritance-flaggene.
Dette indikerer at en ønsker at tillatelsen vår skal spres til alle underordnede kataloger og filer.

ContainerInherit - Forplant til underordnede beholdere (kataloger).
None - Ikke spre denne tillatelsen til noen underordnede beholdere eller objekter.
ObjectInherit - Forplante seg til underordnede objekter (filer).

Propagation Flags
I tillegg til arveflagg kan vi også sette propagation.
Standard i Windows None.
Tre alternativene for propagation flags. 
Hvis du setter dette flagget til InheritOnly, vil tilgangsregelen kun gjelde for underordnede objekter i beholderen.

InheritOnly - Spesifiserer at ACE-en bare spres til underordnede objekter.
None - No inheritance flagg satt.
NoPropagateInherit - ACE går ikke nedover til underelementer.

Flag Combinations and Effect
The table below details how a rule will be apply to the current object and child objects based on the flags set in the access rule.


Access Rule Applies To                  Inheritance Flags                   Propagation Flags
Subfolders and Files only               ContainerInherit,ObjectInherit      InheritOnly
This Folder, Subfolders and Files       ContainerInherit,ObjectInherit	    None
This Folder, Subfolders and Files       ContainerInherit,ObjectInherit	    NoPropagateInherit
This folder and Subfolders	            ContainerInherit	                None
Subfolders only	Container               Inherit                             InheritOnly
This Folder and Files                   ObjectInherit                       None
This Folder and Files                   ObjectInherit                       NoPropagateInherit
This Folder only                        None                                None

GUI: Inheritance Flags

#>



<# 
Setter variabelen $folders til verdien "C:\shares\dev".
Henter ACL for mappen "\core\files\dev".
Lager en ny FileSystemAccessRule-regel kalt $AccessRule med identiteten 
"core\l_fullaccess_DEVshare", med full kontroll tillatelse og tillatelsen "Allow".
Legger til $AccessRule i ACL-en.
Oppdaterer ACL-en for mappen "\core\files\dev".
#>
$folders = ('C:\shares\dev')
$acl = Get-Acl \\core\files\dev
$AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("core\l_fullaccess_DEVshare","FullControl","Allow")
$acl.SetAccessRule($AccessRule)
$ACL | Set-Acl -Path "\\core\files\dev"

<#
Henter oppdatert ACL for mappen "\core\files\dev".
Beskytter ACL-en slik at den ikke kan endres ($ACL.SetAccessRuleProtection($true,$true)).
Oppdaterer den beskyttede ACL-en for mappen "\core\files\dev".
#>
$ACL = Get-Acl -Path "\\core\files\dev"
$ACL.SetAccessRuleProtection($true,$true)
$ACL | Set-Acl -Path "\\core\files\dev"

<#
$acl.SetAccessRuleProtection-metoden tar to argumenter: $True og $False. 
    - De to argumentene bestemmer hvordan tilgangsreglene skal håndteres.
$acl.SetAccessRuleProtection($True,$True):
    - betyr at tilgangsregler vil bli arvet, men ikke kan endres eller slettes.
$acl.SetAccessRuleProtection($False,$False): 
    - betyr at tilgangsregler vil bli arvet og kan endres eller slettes.
$acl.SetAccessRuleProtection($True,$False):
    - betyr at tilgangsregler vil bli arvet, men kan endres. De kan ikke slettes.
$acl.SetAccessRuleProtection($False,$True)
    - betyr at tilgangsregler ikke vil bli arvet, men kan endres eller slettes.
#>


<#
Henter ACL for mappen "\core\files\dev".
Fjerner spesifikke tillatelser for gruppen "BUILTIN\Users".
Oppdaterer ACL-en for mappen "\core\files\dev".
Viser en tabell med identitetsreferanse, filsystemrettigheter, kontrolltype,
om det er arvet og arveflag for den oppdaterte ACL-en.
#>
$acl = Get-Acl "\\core\files\dev"
$acl.Access | Where-Object {$_.IdentityReference -eq "BUILTIN\Users" } | ForEach-Object { $acl.RemoveAccessRuleSpecific($_) }
Set-Acl "\\core\files\dev" $acl
(Get-ACL -Path "\\core\files\dev").Access | 
    Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize


#### - Leksjon 05 - Video 3 - Installer DFS Replication på DC1 for replikering av shared folders ####
# https://learn.microsoft.com/en-us/powershell/module/dfsr/?view=windowsserver2022-ps 

# Installeres på DC1
Install-WindowsFeature FS-DFS-Replication -IncludeManagementTools 

# Utføres på SRV1
New-DfsReplicationGroup -GroupName "RepGrpHRShare" 
Add-DfsrMember -GroupName "RepGrpHRShare" -ComputerName "srv1","dc1" 
Add-DfsrConnection -GroupName "RepGrpHRShare" `
                    -SourceComputerName "srv1" `
                    -DestinationComputerName "dc1" 

New-DfsReplicatedFolder -GroupName "RepGrpHRShare" -FolderName "ReplicaHRSharedFolder" 

Set-DfsrMembership -GroupName "RepGrpHRShare" `
                    -FolderName "hr" `
                    -ContentPath "C:\shares\hr" `
                    -ComputerName "srv1" `
                    -PrimaryMember $True 

Set-DfsrMembership -GroupName "RepGrpHRShare" `
                    -FolderName "ReplicaHRSharedFolder" `
                    -ContentPath "c:\ReplicaHRSharedFolder" `
                    -ComputerName "dc1" 

Get-DfsrCloneState 
