$globalGroups = @('g_it', 'g_hr', 'g_finance', 'g_sales', 'g_dev')

$localGroupName = @('it', 'hr', 'finance', 'sales', 'dev')



# --- Create local group name l_fullaccess_DEVshare --- #
New-ADGroup -GroupCategory Security `
    -GroupScope DomainLocal `
    -Name 'l_fullaccess_DEVshare' `
    -Path 'OU=learnIT_groups,DC=core,DC=sec' `
    -SamAccountName 'l_fullaccess_DEVshare'

Add-ADPrincipalGroupMembership -Identity 'g_dev' -MemberOf 'l_fullaccess_DEVshare'
Get-Help 'Add-ADPrincipalGroupMembership' -Online



# --- Access Control on shared folde --- #
# Kun for utlisting av eksisterende tilgang :)
$folders = "\\core.sec\files\dev"
Get-SmbShareAccess -name 'dev'
Get-Acl -Path $folders
(Get-Acl -Path $folders).Access
(Get-Acl -Path $folders).Access | Format-Table -AutoSize
(Get-Acl -Path $folders).Access | Where-Object {$_.IsInherited -eq $true} | Format-Table -AutoSize
(Get-ACL -Path $folders).Access | Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize



$folders = ('C:\shares\dev')
$acl = Get-Acl \\core\files\dev
$AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("core\l_fullaccess_DEVshare","FullControl","Allow")
$acl.SetAccessRule($AccessRule)
$ACL | Set-Acl -Path "\\core\files\dev"

<#
Henter oppdatert ACL for mappen "\core\files\dev".
Beskytter ACL-en slik at den ikke kan endres ($ACL.SetAccessRuleProtection($true,$true)).
Oppdaterer den beskyttede ACL-en for mappen "\core\files\dev".
#>
$ACL = Get-Acl -Path "\\core\files\dev"
$ACL.SetAccessRuleProtection($true,$true)
$ACL | Set-Acl -Path "\\core\files\dev"


<#
Henter ACL for mappen "\core\files\dev".
Fjerner spesifikke tillatelser for gruppen "BUILTIN\Users".
Oppdaterer ACL-en for mappen "\core\files\dev".
Viser en tabell med identitetsreferanse, filsystemrettigheter, kontrolltype,
om det er arvet og arveflag for den oppdaterte ACL-en.
#>
$acl = Get-Acl "\\core\files\dev"
$acl.Access | Where-Object {$_.IdentityReference -eq "BUILTIN\Users" } | ForEach-Object { $acl.RemoveAccessRuleSpecific($_) }
Set-Acl "\\core\files\dev" $acl
(Get-ACL -Path "\\core\files\dev").Access | 
    Format-Table IdentityReference,FileSystemRights,AccessControlType,IsInherited,InheritanceFlags -AutoSize
